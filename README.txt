SITE MAP PANELS
-------------------------

CONTENTS OF THIS FILE
------------------------------------

 * Introduction
 * Requirements
 * Installation and configuration

INTRODUCTION
-----------------------
Module Site Map Panels depends on the Site Map module and provides 5 ctools 
panes to display a sitemap of the:
* menu's trees
* blogs
* books
* FAQs
* taxonomies.


REQUIREMENTS
------------------------
This module requires the following modules to be installed:
 * Site Map (http://www.drupal.org/project/site_map)
 * Ctools (http://www.drupal.org/project/ctools)

INSTALLATION AND CONFIGURATION
-------------------------------------------------------
1 - Download the module and copy it into modules folder and enable it
from the modules administration/management page.
2 - Configure Site Map module (admin/config/search/sitemap). For example, 
choose content types to display in sitemap.
3 - Add on your panel one or several sitemap's panes (Add content -> Miscellaneous).
4 - If you want to configure access to you sitemap panels you can change 
access in the settings of your panel (Settings -> Access). By default sitemap panes are accessible
for anonymous users.
